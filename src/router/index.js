import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../components/home/HomeContent.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/courses/draft',
    name: 'course.draft',
    component: () => import('../views/courses/Draft.vue')
  },
  {
    path: '/courses/published',
    name: 'course.published',
    component: () => import('../views/courses/Published.vue')
  },
  {
    path: '/course/create',
    name: 'course.create',
    component: () => import('../views/courses/CourseVideoUpload.vue')
  },
  {
    path: '/course/basicinfo',
    name: 'course.basicinfo',
    component: () => import('../views/courses/CourseBasicInfo.vue')
  },
  {
    path: '/course/course-title',
    name: 'course.view',
    component: () => import('../views/courses/CourseView.vue')
  },
  {
    path: '/course/course-title/section-id',
    name: 'course.enroll',
    component: () => import('../views/courses/CourseEnroll.vue')
  },
]

const router = new VueRouter({
  mode: 'history', // add this line to remove hastag(#) from url
  routes
})

export default router
